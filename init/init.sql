DROP TABLE IF EXISTS wallets, transactions;

CREATE TABLE wallets (
	id SERIAL PRIMARY KEY,
	balance DECIMAL DEFAULT 100.0
);

CREATE TABLE transactions (
	id SERIAL PRIMARY KEY,
	datetime TIMESTAMP,
	from_id INTEGER,
	to_id INTEGER,
	amount DECIMAL
);