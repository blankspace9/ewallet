package domain

import (
	"fmt"

	"github.com/shopspring/decimal"
)

type Wallet struct {
	ID      int64
	Balance decimal.Decimal
}

func (w Wallet) MarshalJSON() ([]byte, error) {
	balance, _ := w.Balance.Float64()
	return []byte(fmt.Sprintf(`{"id":%d,"balance":%.2f}`, w.ID, balance)), nil
}
