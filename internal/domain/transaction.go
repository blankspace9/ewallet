package domain

import (
	"time"

	"github.com/shopspring/decimal"
)

type Transaction struct {
	ID       int64           `json:"id"`
	DateTime time.Time       `json:"time"`
	FromID   int64           `json:"from"`
	ToID     int64           `json:"to"`
	Amount   decimal.Decimal `json:"amount"`
}
