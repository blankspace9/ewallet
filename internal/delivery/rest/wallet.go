package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/ewallet/internal/domain"
)

func (h *Handler) createWallet(w http.ResponseWriter, r *http.Request) {
	var wallet domain.Wallet
	wallet.Balance = decimal.NewFromFloat(100.0)

	id, err := h.walletService.CreateWallet(r.Context(), wallet)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		log.Error("Wallets - createWallet - Create wallet error:", err)
		return
	}
	wallet.ID = id

	response, err := json.Marshal(wallet)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		log.Error("Wallets - createWallet - Create wallet error:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func (h *Handler) getWallet(w http.ResponseWriter, r *http.Request) {
	var wallet *domain.Wallet

	idString := mux.Vars(r)["walletId"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Error("Wallets - getWallet - Failed to parse id:", err)
		return
	}

	wallet, err = h.walletService.GetWallet(r.Context(), int64(id))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Error("Wallets - getWallet - Get wallet error:", err)
		return
	}

	response, err := json.Marshal(&wallet)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Error("Wallets - getWallet - Marshal response error:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}
