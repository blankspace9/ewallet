package rest

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/blankspace9/ewallet/internal/service"
)

type Handler struct {
	walletService      service.Wallet
	transactionService service.Transaction
}

func NewHandler(services service.Services) *Handler {
	return &Handler{
		walletService:      services.Wallet,
		transactionService: services.Transaction,
	}
}

func (h *Handler) InitRouter() *mux.Router {
	r := mux.NewRouter()

	api := r.PathPrefix("/api").Subrouter()
	{
		v1 := api.PathPrefix("/v1").Subrouter()
		{
			wallet := v1.PathPrefix("/wallet").Subrouter()
			{
				wallet.HandleFunc("", h.createWallet).Methods(http.MethodPost)
				wallet.HandleFunc("/{walletId}", h.getWallet).Methods(http.MethodGet)
				wallet.HandleFunc("/{walletId}/send", h.transaction).Methods(http.MethodPatch)
				wallet.HandleFunc("/{walletId}/history", h.history).Methods(http.MethodGet)
			}
		}
	}

	return r
}
