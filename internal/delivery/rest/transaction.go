package rest

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/service/errs"
)

func (h *Handler) transaction(w http.ResponseWriter, r *http.Request) {
	fromString := mux.Vars(r)["walletId"]
	fromID, err := strconv.Atoi(fromString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Error("Transaction - transaction - Failed to parse id:", err)
		return
	}

	var transaction domain.Transaction

	d := json.NewDecoder(r.Body)

	err = d.Decode(&transaction)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Error("Transaction - transaction - Failed to parse JSON:", err)
		return
	}
	transaction.FromID = int64(fromID)

	_, err = h.transactionService.MakeTransaction(r.Context(), transaction)
	if err != nil {
		if err == errs.ErrSourceNotFound {
			w.WriteHeader(http.StatusNotFound)
		} else if err == errs.ErrDestNotFound || err == errs.ErrNotEnoughMoney {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		log.Error("Transaction - transaction - Failed to make a transaction:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *Handler) history(w http.ResponseWriter, r *http.Request) {
	sourceString := mux.Vars(r)["walletId"]
	sourceID, err := strconv.Atoi(sourceString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Error("Transaction - history - Failed to parse id:", err)
		return
	}

	transactions, err := h.transactionService.GetHistory(r.Context(), int64(sourceID))
	if err != nil {
		if err == errs.ErrSourceNotFound {
			w.WriteHeader(http.StatusBadRequest)
		}
	}

	resp, err := json.Marshal(map[string][]domain.Transaction{
		"history": transactions,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Error("Transaction - history - Failed to marshal response:", err)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(resp)
}
