package app

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/ewallet/internal/config"
	"gitlab.com/blankspace9/ewallet/internal/delivery/rest"
	"gitlab.com/blankspace9/ewallet/internal/repository"
	"gitlab.com/blankspace9/ewallet/internal/service"
	"gitlab.com/blankspace9/ewallet/pkg/httpserver"
	"gitlab.com/blankspace9/ewallet/pkg/postgres"
)

func Run(folder, filename string) {
	cfg, err := config.NewConfig(folder, filename)
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	SetLogger(cfg.Log.Level)

	// init postgres connection
	log.Info("Init postgres db...")
	pg, err := postgres.NewPostgresConnection(postgres.PostgresConnectionInfo{
		Host:     cfg.DB.Host,
		Port:     cfg.DB.Port,
		Username: cfg.DB.Username,
		DBName:   cfg.DB.DB,
		SSLMode:  cfg.DB.SSLMode,
		Password: cfg.DB.Password,
	})
	if err != nil {
		log.Fatalf("Run app - Init postgres db: %s", err)
	}
	defer pg.Close()

	// init repositories
	log.Info("Initializing repositories...")
	repositories := repository.NewRepositories(pg)

	// init services
	log.Info("Initializing dependencies...")
	deps := service.ServiceDependencies{
		Repos: repositories,
	}

	log.Info("Initializing services...")
	services := service.NewServices(deps)

	// init handler
	log.Info("Initializing handler...")
	handler := rest.NewHandler(*services)

	// init & run server
	log.Info("Starting http server...")
	log.Debugf("Server port: %s", cfg.Server.Port)
	httpServer := httpserver.NewServer(handler.InitRouter(), httpserver.Port(cfg.Server.Port))

	log.Info("Server started")

	// graceful shutdown
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-exit:
		log.Info("Run app - signal: " + s.String())
	case err = <-httpServer.Notify():
		log.Errorf("Run app - httpServer.Notify: %s", err)
	}

	log.Info("Shutting down...")
	err = httpServer.Shutdown()
	if err != nil {
		log.Errorf("Run app - httpServer.Shutdown: %s", err)
	}

	log.Info("Server gracefully stopped")
}
