package errs

import (
	"errors"
)

var (
	ErrNotEnoughMoney      = errors.New("not enough money")
	ErrSourceNotFound      = errors.New("source wallet not found")
	ErrDestNotFound        = errors.New("destination wallet not found")
	ErrWalletAlreadyExists = errors.New("wallet already exists")
	ErrCannotCreateWallet  = errors.New("cannot create wallet")
	ErrWalletNotFound      = errors.New("wallet not found")
	ErrCannotGetWallet     = errors.New("cannot get wallet")
)
