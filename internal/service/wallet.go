package service

import (
	"context"

	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/repository"
)

type WalletService struct {
	walletRepo repository.Wallet
}

func NewWalletService(walletRepo repository.Wallet) *WalletService {
	return &WalletService{walletRepo}
}

func (ws *WalletService) CreateWallet(ctx context.Context, wallet domain.Wallet) (int64, error) {
	id, err := ws.walletRepo.Create(ctx, wallet)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (ws *WalletService) GetWallet(ctx context.Context, id int64) (*domain.Wallet, error) {
	wallet, err := ws.walletRepo.Get(ctx, id)
	if err != nil {
		return nil, err
	}

	return wallet, nil
}
