package service

import (
	"context"

	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/repository"
)

type Wallet interface {
	CreateWallet(ctx context.Context, wallet domain.Wallet) (int64, error)
	GetWallet(ctx context.Context, id int64) (*domain.Wallet, error)
}

type Transaction interface {
	MakeTransaction(ctx context.Context, transaction domain.Transaction) (int64, error)
	GetHistory(ctx context.Context, id int64) ([]domain.Transaction, error)
}

type ServiceDependencies struct {
	Repos *repository.Repositories
}

type Services struct {
	Wallet
	Transaction
}

func NewServices(deps ServiceDependencies) *Services {
	return &Services{
		Wallet:      NewWalletService(deps.Repos.Wallet),
		Transaction: NewTransactionService(deps.Repos.Wallet),
	}
}
