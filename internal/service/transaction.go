package service

import (
	"context"

	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/repository"
	"gitlab.com/blankspace9/ewallet/internal/service/errs"
)

type TransactionService struct {
	walletRepo repository.Wallet
}

func NewTransactionService(walletRepo repository.Wallet) *TransactionService {
	return &TransactionService{walletRepo}
}

func (ts *TransactionService) MakeTransaction(ctx context.Context, transaction domain.Transaction) (int64, error) {
	fromWallet, err := ts.walletRepo.Get(ctx, transaction.FromID)
	if err != nil {
		return -1, errs.ErrSourceNotFound
	}
	if fromWallet.Balance.LessThan(transaction.Amount) {
		return -1, errs.ErrNotEnoughMoney
	}

	toWallet, err := ts.walletRepo.Get(ctx, transaction.ToID)
	if err != nil {
		return -1, errs.ErrDestNotFound
	}

	id, err := ts.walletRepo.Transaction(ctx, *fromWallet, *toWallet, transaction.Amount)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (ts *TransactionService) GetHistory(ctx context.Context, id int64) ([]domain.Transaction, error) {
	_, err := ts.walletRepo.Get(ctx, id)
	if err != nil {
		return nil, errs.ErrSourceNotFound
	}

	transactions, err := ts.walletRepo.History(ctx, id)
	if err != nil {
		return nil, err
	}

	return transactions, nil
}
