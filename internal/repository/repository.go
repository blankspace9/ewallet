package repository

import (
	"context"
	"database/sql"

	"github.com/shopspring/decimal"
	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/repository/postgres"
)

type Wallet interface {
	Create(ctx context.Context, wallet domain.Wallet) (int64, error)
	Get(ctx context.Context, id int64) (*domain.Wallet, error)
	Transaction(ctx context.Context, from, to domain.Wallet, amount decimal.Decimal) (int64, error)
	History(ctx context.Context, id int64) ([]domain.Transaction, error)
}

type Repositories struct {
	Wallet
}

func NewRepositories(db *sql.DB) *Repositories {
	return &Repositories{
		Wallet: postgres.NewWalletRepo(db),
	}
}
