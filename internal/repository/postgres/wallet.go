package postgres

import (
	"context"
	"database/sql"
	"time"

	"github.com/lib/pq"
	"github.com/shopspring/decimal"
	"gitlab.com/blankspace9/ewallet/internal/domain"
	"gitlab.com/blankspace9/ewallet/internal/repository/repoerrors"
)

type WalletRepo struct {
	db *sql.DB
}

func NewWalletRepo(db *sql.DB) *WalletRepo {
	return &WalletRepo{db}
}

func (sr *WalletRepo) Create(ctx context.Context, wallet domain.Wallet) (int64, error) {
	var id int64
	err := sr.db.QueryRow("INSERT INTO wallets DEFAULT VALUES RETURNING id").Scan(&id)
	if err != nil {
		pqErr, ok := err.(*pq.Error)
		if ok && pqErr.Code == "23505" {
			return -1, repoerrors.ErrAlreadyExist
		}
		return -1, err
	}

	return id, nil
}

func (sr *WalletRepo) Get(ctx context.Context, id int64) (*domain.Wallet, error) {
	var wallet domain.Wallet

	err := sr.db.QueryRow("SELECT id, balance FROM wallets WHERE id=$1", id).Scan(&wallet.ID, &wallet.Balance)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, repoerrors.ErrNotFound
		}
		return nil, err
	}

	return &wallet, nil
}

func (wr *WalletRepo) Transaction(ctx context.Context, from, to domain.Wallet, amount decimal.Decimal) (int64, error) {
	var id int64

	tx, err := wr.db.Begin()
	if err != nil {
		return -1, err
	}

	_ = wr.db.QueryRow("UPDATE wallets SET balance=balance-$1 WHERE id=$2", amount, from.ID)
	_ = wr.db.QueryRow("UPDATE wallets SET balance=balance+$1 WHERE id=$2", amount, to.ID)

	err = wr.db.QueryRow("INSERT INTO transactions (datetime, from_id, to_id, amount) values ($1, $2, $3, $4) RETURNING id", pq.FormatTimestamp(time.Now()), from.ID, to.ID, amount).Scan(&id)
	if err != nil {
		return -1, err
	}

	err = tx.Commit()
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (wr *WalletRepo) History(ctx context.Context, id int64) ([]domain.Transaction, error) {
	var transactions []domain.Transaction
	var transaction domain.Transaction

	rows, err := wr.db.Query("SELECT id, datetime, from_id, to_id, amount FROM transactions WHERE from_id=$1 OR to_id=$2", id, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&transaction.ID, &transaction.DateTime, &transaction.FromID, &transaction.ToID, &transaction.Amount)
		if err != nil {
			return nil, err
		}
		transactions = append(transactions, transaction)
	}

	return transactions, nil
}
