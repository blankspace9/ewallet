# ewallet

Сервис обработки транзакций платёжной системы.  
  
Используемые технологии:
- PostgreSQL
- Docker
- gorilla/mux
- pq
- logrus
- viper
  
Сервис был написан с использованием чистой архитектуры. Также был реализован Graceful Shutdown для корректного завершения работы.

## Getting started

Запустить сервис можно с помощью команды `docker-compose up --build -d && docker-compose logs -f`

## Examples  
#### Создание кошелька
Запрос
``````
curl --location --request POST 'localhost:8080/api/v1/wallet' \
--data ''
``````  
Ответ  
``````
{
    "id": 3,
    "balance": 100.00
}
``````
#### Получение состояния кошелька 
Запрос
``````
curl --location 'localhost:8080/api/v1/wallet/3' \
--data ''
``````  
Ответ
``````
{
    "id": 3,
    "balance": 100.00
}
``````  
#### Транзакция 
Запрос
``````
curl --location --request PATCH 'localhost:8080/api/v1/wallet/3/send' \
--header 'Content-Type: application/json' \
--data '{
    "to": 4,
    "amount": 22.5
}'
``````  
Ответ 
``````
Status 200
``````
Кошелек 3
``````
{
    "id": 3,
    "balance": 77.50
}
``````  
Кошелек 4
``````
{
    "id": 4,
    "balance": 122.50
}
``````  
#### Получение истории транзакци
Запрос
``````
curl --location 'localhost:8080/api/v1/wallet/3/history' \
--data ''
``````  
Ответ 
``````
{
    "history": [
        {
            "id": 2,
            "time": "2024-01-23T17:03:38.292221Z",
            "from": 3,
            "to": 4,
            "amount": "22.5"
        }
    ]
}
``````  
